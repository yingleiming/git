| 描述                                                 | 命令                                            |
| ---------------------------------------------------- | ----------------------------------------------- |
| 查看本地仓库配置                                     | `git confit --local -l`                         |
| 查看用户配置                                         | `git config --global -l`                        |
| 查看系统配置                                         | `git config --system -l`                        |
| 查看所有配置                                         | `git config --list`                             |
| 重置用户名                                           | `git config --global user.name "userName"`      |
| 重置邮箱                                             | `git config –global user.emai "user@email.com"` |
| 初始化一个git仓库                                    | `git init`                                      |
| 新建一个文件夹，并初始化一个git仓库                  | `git init projectName`                          |
| 初始化一个裸仓库                                     | `git init --bare`                               |
| 添加一个或多个文件到暂存区                           | `git add file1 file2 file3...`                  |
| 添加指定目录到暂存区，包含子目录                     | `git add [dir]`                                 |
| 添加当前目录下的所有文件到暂存区，不包括被删除的文件 | `git add .`                                     |
|                                                      | `git add -a`                                    |
|                                                      |                                                 |
|                                                      |                                                 |
|                                                      |                                                 |
|                                                      |                                                 |



